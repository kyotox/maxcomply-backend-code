<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230724205649 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE vehicle (id INT AUTO_INCREMENT NOT NULL, manufacturer_id INT NOT NULL, model_name VARCHAR(48) NOT NULL, year INT NOT NULL, INDEX IDX_1B80E486A23B42D (manufacturer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vehicle_parameter (id INT AUTO_INCREMENT NOT NULL, vehicle_id INT NOT NULL, type VARCHAR(48) NOT NULL, value VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_DD28D442545317D1 (vehicle_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE vehicle ADD CONSTRAINT FK_1B80E486A23B42D FOREIGN KEY (manufacturer_id) REFERENCES manufacturer (id)');
        $this->addSql('ALTER TABLE vehicle_parameter ADD CONSTRAINT FK_DD28D442545317D1 FOREIGN KEY (vehicle_id) REFERENCES vehicle (id)');
        $this->addSql('ALTER TABLE manufacturer ADD name VARCHAR(48) NOT NULL, ADD email VARCHAR(48) NOT NULL, ADD phone VARCHAR(48) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE vehicle DROP FOREIGN KEY FK_1B80E486A23B42D');
        $this->addSql('ALTER TABLE vehicle_parameter DROP FOREIGN KEY FK_DD28D442545317D1');
        $this->addSql('DROP TABLE vehicle');
        $this->addSql('DROP TABLE vehicle_parameter');
        $this->addSql('ALTER TABLE manufacturer DROP name, DROP email, DROP phone');
    }
}
