<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\VehicleParameterRepository;
use App\Validator\Constraints\MaximumVehicleParameters;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: VehicleParameterRepository::class)]
#[ORM\HasLifecycleCallbacks]
#[ApiResource()]
class VehicleParameter
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 48)]
    #[Groups(['vehicle.read'])]
    private ?string $type = null;

    #[ORM\Column(length: 255)]
    #[Groups(['vehicle.read'])]
    private ?string $value = null;

    #[ORM\ManyToOne(inversedBy: 'vehicleParameters')]
    #[ORM\JoinColumn(nullable: false)]
    #[MaximumVehicleParameters]
    private ?Vehicle $vehicle = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    #[Groups(['vehicle.parameters.read'])]
    private ?\DateTimeInterface $createdAt = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): static
    {
        $this->type = $type;

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): static
    {
        $this->value = $value;

        return $this;
    }

    public function getVehicle(): ?Vehicle
    {
        return $this->vehicle;
    }

    public function setVehicle(?Vehicle $vehicle): static
    {
        $this->vehicle = $vehicle;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    #[ORM\PrePersist]
    public function setCreatedAtValue(): void
    {
        $this->createdAt = new \DateTimeImmutable();
    }
}
