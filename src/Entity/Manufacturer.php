<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\ManufacturerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: ManufacturerRepository::class)]
#[ApiResource(
    normalizationContext: ['groups' => ['manufacturer.read']],
    denormalizationContext: ['groups' => ['manufacturer.write']],
)]
class Manufacturer
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['vehicle.read', 'manufacturer.read'])]
    private ?int $id = null;

    #[ORM\Column(length: 48)]
    #[Groups(['vehicle.read', 'manufacturer.write', 'manufacturer.read'])]
    private ?string $name = null;

    #[ORM\Column(length: 48)]
    #[Groups(['manufacturer.write'])]
    private ?string $email = null;

    #[ORM\Column(length: 48, nullable: true)]
    #[Groups(['manufacturer.write'])]
    private ?string $phone = null;

    #[ORM\OneToMany(mappedBy: 'manufacturer', targetEntity: Vehicle::class)]
    #[Groups(['manufacturer.read'])]
    private Collection $vehicles;

    public function __construct()
    {
        $this->vehicles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): static
    {
        $this->email = $email;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): static
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return Collection<int, Vehicle>
     */
    public function getVehicles(): Collection
    {
        return $this->vehicles;
    }

    public function addVehicle(Vehicle $vehicle): static
    {
        if (!$this->vehicles->contains($vehicle)) {
            $this->vehicles->add($vehicle);
            $vehicle->setManufacturer($this);
        }

        return $this;
    }

    public function removeVehicle(Vehicle $vehicle): static
    {
        if ($this->vehicles->removeElement($vehicle)) {
            // set the owning side to null (unless already changed)
            if ($vehicle->getManufacturer() === $this) {
                $vehicle->setManufacturer(null);
            }
        }

        return $this;
    }
}
