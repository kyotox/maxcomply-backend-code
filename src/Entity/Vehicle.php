<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiProperty;
use App\Validator\Constraints\MaximumVehicleParameters;
use App\Repository\VehicleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Metadata\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: VehicleRepository::class)]
#[ApiResource(
    normalizationContext: ['groups' => ['vehicle.read']],
    denormalizationContext: ['groups' => ['vehicle.write']],
)]
#[ApiFilter(SearchFilter::class, properties: ['id' => 'exact', 'modelName' => 'partial', 'type' => 'partial'])]
class Vehicle
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 48)]
    #[Groups(['vehicle.read', 'vehicle.write'])]
    private ?string $modelName = null;

    #[ORM\ManyToOne(inversedBy: 'vehicles')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['vehicle.read', 'vehicle.write'])]
    #[ApiProperty(readableLink: true, writableLink: true)]
    private ?Manufacturer $manufacturer = null;

    #[ORM\Column]
    #[Groups(['vehicle.read', 'vehicle.write'])]
    private ?int $year = null;

    #[ORM\OneToMany(mappedBy: 'vehicle', targetEntity: VehicleParameter::class, orphanRemoval: true)]
    #[Groups(['vehicle.read'])]
    private Collection $vehicleParameters;

    #[ORM\Column(length: 48,  enumType: VehicleType::class)]
    #[Groups(['vehicle.read', 'vehicle.write'])]
    private VehicleType|null $type = null;

    public function __construct()
    {
        $this->type = VehicleType::Car;
        $this->vehicleParameters = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getModelName(): ?string
    {
        return $this->modelName;
    }

    public function setModelName(string $modelName): static
    {
        $this->modelName = $modelName;

        return $this;
    }

    public function getManufacturer(): ?Manufacturer
    {
        return $this->manufacturer;
    }

    public function setManufacturer(?Manufacturer $manufacturer): static
    {
        $this->manufacturer = $manufacturer;

        return $this;
    }

    public function getYear(): ?int
    {
        return $this->year;
    }

    public function setYear(int $year): static
    {
        $this->year = $year;

        return $this;
    }

    /**
     * @return Collection<int, VehicleParameter>
     */
    public function getVehicleParameters(): Collection
    {
        return $this->vehicleParameters;
    }

    public function addVehicleParameter(VehicleParameter $vehicleParameter): static
    {
        if (!$this->vehicleParameters->contains($vehicleParameter)) {
            $this->vehicleParameters->add($vehicleParameter);
            $vehicleParameter->setVehicle($this);
        }

        return $this;
    }

    public function removeVehicleParameter(VehicleParameter $vehicleParameter): static
    {
        if ($this->vehicleParameters->removeElement($vehicleParameter)) {
            // set the owning side to null (unless already changed)
            if ($vehicleParameter->getVehicle() === $this) {
                $vehicleParameter->setVehicle(null);
            }
        }

        return $this;
    }

    public function getType(): VehicleType
    {
        return $this->type;
    }

    public function setType(VehicleType $type): static
    {
        $this->type = $type;

        return $this;
    }
}
