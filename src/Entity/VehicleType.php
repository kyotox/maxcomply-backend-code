<?php

namespace App\Entity;

enum VehicleType: string
{
    case Car = "Car";
    case Motorcycle = "Motorcycle";
    case Hovercraft = "Hovercraft";
}
