<?php

namespace App\Repository;

use App\Entity\VehicleParameter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<VehicleParameter>
 *
 * @method VehicleParameter|null find($id, $lockMode = null, $lockVersion = null)
 * @method VehicleParameter|null findOneBy(array $criteria, array $orderBy = null)
 * @method VehicleParameter[]    findAll()
 * @method VehicleParameter[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VehicleParameterRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, VehicleParameter::class);
    }

//    /**
//     * @return VehicleParameter[] Returns an array of VehicleParameter objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('v')
//            ->andWhere('v.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('v.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?VehicleParameter
//    {
//        return $this->createQueryBuilder('v')
//            ->andWhere('v.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
