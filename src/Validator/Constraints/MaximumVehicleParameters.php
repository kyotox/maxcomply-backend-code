<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
#[\Attribute]
class MaximumVehicleParameters extends Constraint
{
    public $message = 'The vehicle can have maximum 10 parameters';
}