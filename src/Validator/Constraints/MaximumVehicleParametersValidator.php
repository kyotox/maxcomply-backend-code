<?php

namespace App\Validator\Constraints;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
final class MaximumVehicleParametersValidator extends ConstraintValidator
{
    public function validate($vehicle, Constraint $constraint): void
    {
        if ($vehicle->getVehicleParameters()->count() > 10) {
            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }
}